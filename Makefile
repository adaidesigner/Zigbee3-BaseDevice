#
# Zigbee3.0工程编译
#

.PHONY: all clean router coordinator enddevice

SERIAL_PORT_ROUTER := COM9
SERIAL_PORT_COORDINATOR := COM5

LDFLAGS = JENNIC_CHIP_FAMILY=JN516x JENNIC_CHIP=JN5169 DR=DR1199 GROUPS=0 ICODES=0

all: enddevice router coordinator

info-router:
	JN51xxProgrammer.exe -s ${SERIAL_PORT_ROUTER} --deviceconfig -V 0

info-coordinator:
	JN51xxProgrammer.exe -s ${SERIAL_PORT_COORDINATOR} --deviceconfig -V 0

erase-router:
	JN51xxProgrammer.exe -s ${SERIAL_PORT_ROUTER} --eraseeeprom=full -V 0

erase-coordinator:
	JN51xxProgrammer.exe -s ${SERIAL_PORT_COORDINATOR} --eraseeeprom=full -V 0

flash-router:
	JN51xxProgrammer.exe -s ${SERIAL_PORT_ROUTER} -f Router/Build/Router_JN5169_DR1199.bin -V 0

flash-coordinator:
	JN51xxProgrammer.exe -s ${SERIAL_PORT_COORDINATOR} -f Coordinator/Build/Coordinator_JN5169_DR1199.bin -V 0

coordinator:
	cd Coordinator/Build && make $(LDFLAGS)

router:
	cd Router/Build && make $(LDFLAGS)

enddevice:
	cd EndDevice/Build && make $(LDFLAGS)

clean:
	cd Coordinator/Build && make $(LDFLAGS) clean
	cd Router/Build && make $(LDFLAGS) clean
	cd EndDevice/Build && make $(LDFLAGS) clean










